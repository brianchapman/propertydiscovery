package main

import (
	"database/sql"
	"fmt"
	"os"
	_ "github.com/go-sql-driver/mysql"
	"github.com/tealeg/xlsx"
)

func main() {
	excelFileName := os.Getenv("EXCEL_FILE")
	xlFile, xlserr := xlsx.OpenFile(excelFileName)
	howMany := 0
	if xlserr == nil {
		db, dberr := sql.Open("mysql", os.Getenv("MYSQL_USERNAME") + ":" + os.Getenv("MYSQL_PASSWORD") + "@tcp(" + os.Getenv("MYSQL_HOSTNAME") + ":3306)/" + os.Getenv("MYSQL_DB"))
		if dberr == nil {
			defer db.Close()
			for _, sheet := range xlFile.Sheets {
				for _, row := range sheet.Rows {
					howMany++
					if howMany > 1 {
						myParcelID := "1"
						myStreetNum := "1"
						myStreetName := "1"
						whichCol := 0
						for _, cell := range row.Cells {
							whichCol++
							if whichCol == 3 {
								myStreetNum = cell.String()
							}
							if whichCol == 4 {
								myStreetName = cell.String()
							}
							if whichCol == 6 {
								myParcelID = cell.String()
							}
						}
						if myStreetName == "1" || myParcelID == "1" {
							howMany++
						} else {
							insertStatement, err := db.Prepare("INSERT INTO parcels (parcelid, streetnum, streetname) VALUES ( ?, ?, ? )")
							if err != nil {
								fmt.Printf("\nERROR at Prepare on parcelID %s -\n", myParcelID)
								fmt.Println(err)
								// return
							}
							_, err = insertStatement.Exec(myParcelID, myStreetNum, myStreetName)
							if err != nil {
								fmt.Printf("\nERROR at Exec on parcelID %s -\n", myParcelID)
								fmt.Println(err)
							}
							insertStatement.Close()

						}
					}
				}
			}
		}
	}
}
