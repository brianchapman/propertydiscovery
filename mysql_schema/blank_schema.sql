CREATE TABLE `parcels` (
  `id` int(11) NOT NULL,
  `parcelid` varchar(30) NOT NULL,
  `streetnum` varchar(8) NOT NULL,
  `streetname` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

ALTER TABLE `parcels`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `parcelid_uniq` (`parcelid`);

ALTER TABLE `parcels`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=88788;
COMMIT;
